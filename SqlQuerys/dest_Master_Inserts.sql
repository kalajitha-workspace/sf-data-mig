
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AD', N'Andorran')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AE', N'Unit.Arab Emir.')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AF', N'Afghan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AG', N'Antiguan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AI', N'Anguilla')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AL', N'Albanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AM', N'Armenian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AN', N'Dutch')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AO', N'Angolan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AQ', N'Antarctica')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AR', N'Argentinian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AS', N'Samoan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AT', N'Austrian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AU', N'Australian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AW', N'Arubanic')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'AZ', N'Azerbaijani')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BA', N'Bosnian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BB', N'Barbadan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BC', N'Belarus')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BD', N'Bangladeshi')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BE', N'Belgian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BF', N'Burkinabe')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BG', N'Bulgarian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BH', N'Bahraini')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BI', N'Burundi')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BJ', N'Beninese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BM', N'Bermudan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BN', N'Brunei')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BO', N'Bolivian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BR', N'Brazilian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BS', N'Bahaman')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BT', N'Bhutanese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BV', N'Bouvet Islands')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BW', N'Botswanan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BY', N'Belarusian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'BZ', N'Belizean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CA', N'Canadian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CB', N'C�te d�Ivoire')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CC', N'Australian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CD', N'Congolese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CF', N'Central African')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CG', N'Congolese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CH', N'Swiss')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CI', N'Ivoirian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CK', N'Cook Islands')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CL', N'Chilean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CM', N'Cameroonian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CN', N'Chinese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CO', N'Colombian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CR', N'Costa Rican')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CS', N'Serbian/Monten.')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CU', N'Cuban')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CV', N'Cape Verdean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CW', N'Dutch')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CX', N'Australian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CY', N'Cypriot')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'CZ', N'Czech')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'DE', N'German')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'DJ', N'Djiboutian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'DK', N'Danish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'DM', N'Dominican')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'DO', N'Dominican')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'DZ', N'Algerian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'EC', N'Ecuadorian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'EE', N'Estonian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'EG', N'Egyptian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'EH', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'EL', NULL)
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ER', N'Eritrean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ES', N'Spanish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ET', N'Ethiopian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'FI', N'Finnish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'FJ', N'Fijian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'FK', N'British')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'FM', N'Micronesian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'FO', N'Danish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'FR', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GA', N'Gabonese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GB', N'British')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GD', N'Grenadian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GE', N'Georgian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GF', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GG', NULL)
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GH', N'Ghanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GI', N'Gibraltar')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GL', N'Danish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GM', N'Gambian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GN', N'Guinean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GP', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GQ', N'Equatorial Guin')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GR', N'Greek')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GS', N'South Georgia')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GT', N'Guatemalan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GU', N'American Guam')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GW', N'Guinean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'GY', N'Guyanese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'HK', N'Hong Kong')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'HM', N'Heard/McDon.Isl')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'HN', N'Honduran')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'HR', N'Croatian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'HT', N'Haitian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'HU', N'Hungarian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ID', N'Indonesian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IE', N'Irish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IL', N'Israeli')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IM', N'British')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IN', N'Indian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IO', N'Brit.Ind.Oc.Ter')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IQ', N'Iraqi')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IR', N'Iranian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IS', N'Icelandic')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'IT', N'Italian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'JE', N'British')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'JM', N'Jamaican')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'JO', N'Jordanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'JP', N'Japanese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KE', N'Kenyan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KG', N'Kyrgyzstani')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KH', N'Cambodian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KI', N'Kiribati')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KM', N'Comoran')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KN', N'f. St Kitts/Nev')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KP', N'Korean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KR', N'Korean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KW', N'Kuwaiti')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KY', N'British')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'KZ', N'Kazakh')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LA', N'Laotian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LB', N'Lebanese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LC', N'Lucian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LI', N'Liechtenstein')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LK', N'Sri Lankan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LR', N'Liberian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LS', N'Lesothan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LT', N'Lithuanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LU', N'Luxembourgian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LV', N'Latvian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'LY', N'Libyan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MA', N'Moroccan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MC', N'Monegasque')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MD', N'Moldovan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ME', N'Montenegro')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MG', N'Madagascan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MH', N'Marshallese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MK', N'Macedonian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ML', N'Malian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MM', N'Myanmar')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MN', N'Mongolian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MO', N'Portuguese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MP', N'Marianian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MQ', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MR', N'Mauretanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MS', N'Montserrat')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MT', N'Maltese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MU', N'Mauritian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MV', N'Maldivian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MW', N'Malawian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MX', N'Mexican')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MY', N'Malaysian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'MZ', N'Mozambican')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NA', N'Namibian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NC', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NE', N'Nigerien')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NF', N'Norfolk Islands')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NG', N'Nigerian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NI', N'Nicaraguan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NL', N'Dutch')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NO', N'Norwegian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NP', N'Nepalese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NR', N'Nauruian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NU', N'Niuean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'NZ', N'New Zealand')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'OM', N'Omani')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PA', N'Panamanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PE', N'Peruvian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PF', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PG', N'Pap.New Guinean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PH', N'Filipino')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PK', N'Pakistani')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PL', N'Polish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PM', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PN', N'British')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PR', N'American')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PS', N'Palestine')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PT', N'Portuguese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PW', N'Palau')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'PY', N'Paraguayan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'QA', N'Qatari')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'RE', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'RO', N'Rumanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'RS', N'Serbian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'RU', N'Russian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'RW', N'Rwandan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SA', N'Saudi Arabian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SB', N'Solomonese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SC', N'Seychellian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SD', N'Sudanese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SE', N'Swedish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SG', N'Singaporean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SH', N'Saint Helena')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SI', N'Slovenian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SJ', N'Norwegian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SK', N'Slovakian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SL', N'Sierra Leonean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SM', N'Sammarinese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SN', N'Senegalese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SO', N'Somali')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SR', N'Surinamese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SS', N'South Sudanese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ST', N'Sao Tomean')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SV', N'Salvadoran')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SY', N'Syrian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'SZ', N'Swazi')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TC', N'Turksh Caicosin')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TD', N'Chadian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TF', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TG', N'Togolese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TH', N'Thai')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TJ', N'Tajikistani')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TK', N'Tokelau Islands')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TL', N'Timor-Leste or')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TM', N'Turkmenian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TN', N'Tunisian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TO', N'Tongan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TP', N'East Timor')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TR', N'Turkish')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TT', N'F. Trinidad & T')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TV', N'Tuvaluese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TW', N'Taiwanese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'TZ', N'Tanzanian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'UA', N'Ukrainian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'UG', N'Ugandan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'UM', N'Minor Outl.Isl.')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'US', N'American')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'UY', N'Uruguayan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'UZ', N'Uzbekistani')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'VA', N'Vatican City')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'VC', N'Vincentian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'VE', N'Venezuelan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'VG', N'British')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'VI', N'American')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'VN', N'Vietnamese')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'VU', N'Ni-Vanuatu')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'WF', N'Wallis,Futuna')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'WS', N'Samoan')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'XK', N'Kosovo')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'YE', N'Yemeni')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'YT', N'French')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'YU', N'Yugoslavian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ZA', N'South African')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ZM', N'Zambian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ZR', N'Zairian')
GO
INSERT [dbo].[dest_Country_M] ([Country], [Nationality]) VALUES (N'ZW', N'Zimbabwean')



INSERT [dbo].[dest_EmploymentStatus_M] ([EmploymentStatus], [Description]) VALUES (0, N'Withdrawn')
GO
INSERT [dbo].[dest_EmploymentStatus_M] ([EmploymentStatus], [Description]) VALUES (1, N'Inactive')
INSERT [dbo].[dest_EmploymentStatus_M] ([EmploymentStatus], [Description]) VALUES (2, N'Retiree')
GO
INSERT [dbo].[dest_EmploymentStatus_M] ([EmploymentStatus], [Description]) VALUES (3, N'Active')

INSERT [dbo].[dest_EmploymentStatus_M] ([EmploymentStatus], [Description]) VALUES (4, N'Pro-Active')
GO


INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'01', N'Hiring')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'02', N'Organizational reassignment')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'03', N'Transfer/Relocation')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'04', N'Promotion')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'05', N'Confirmation')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'06', N'Leaving')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'08', N'Change in Pay')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'09', N'Extension of Probation')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'12', N'Assignment')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'16', N'Change in pay')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'18', N'Onsite Assignment')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'19', N'Return from Onsite Assignment')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'20', N'Career Grouping')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'26', N'Sabbatical Leave')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'27', N'Return from Sabbatical Leave')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'30', N'Proactive Hiring')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'31', N'Trainee Confirmation')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'32', N'Long Term Absence')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'35', N'Long Absence No Ben.')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'36', N'Return from Leave')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'37', N'WCB Absence')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'38', N'Relegation of career group')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'39', N'Reinstatement of career group')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'40', N'Retirement (Civil servant)')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'41', N'Death of retired civil servant')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'42', N'Incl. surviving deps.'' pension')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'45', N'Organizational reassignment')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'50', N'Time rec.(supplement)')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'51', N'Time recording (mini master)')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'52', N'Org.management (mini master)')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'53', N'Trainee To Employee')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'54', N'Time sheet (mini master)')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'56', N'Hiring (TE mini master)')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'57', N'Org. reassign.(TE mini master)')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'58', N'Fixed Term to Core Employee')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'59', N'Extension of Retirement')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'60', N'Transfer applicant data')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'61', N'Transfer other appl.data')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'62', N'Hire applicant')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'70', N'Mistaken Entry')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'71', N'Corrected entry')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'72', N'Inter Company Code Transfer')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'73', N'Change in Practice')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'80', N'Compensation Action')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'81', N'Expatriation planning')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'82', N'Activation in host country')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'83', N'Expatriation')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'84', N'Change expat. planning data')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'85', N'Trainee Confirmation')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'90', N'Legacy Tag BU Movement')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'91', N'Leave of absence')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'92', N'Return from leave of absence')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'93', N'Transfer Within Wipro Ltd.')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'99', N'Inter BU Movement')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'AC', N'Emp status-Inactive to Active')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'B0', N'Enrollment in Pension Fund')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'B1', N'Deceased in Pension Fund')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'B2', N'Enroll Beneficiary of Deceased')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'B3', N'Withdrawal from Pension Fund')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'B4', N'Retirement Pension Fund')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'BB', N'Display expat. planning data')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'BK', N'Bulk Movement')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'GT', N'Global Transfer')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'IN', N'Emp status-Active to Inactive')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'R1', N'Export to Resumix')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'R2', N'Import from Resumix')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'RA', N'External recruitment')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'RX', N'Resumix integration')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'YB', N'Hiring Service Provider')
GO
INSERT [dbo].[dest_Actions_M] ([Action], [NameOfActionType]) VALUES (N'ZX', N'Intra BU Movement')



--------------------

INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'FTS', N'40', N'WF01', N'Telangana', N'', N'IN', N'36', N'0005993993')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'FUP', N'40', N'WF01', N'Uttar Pradesh', N'', N'IN', N'09', N'0005993994')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'UNZA', N'40', N'WG01', N'UNZA', N'UNZA', N'IN', N'29', N'0006579954')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WARP', N'40', N'WG01', N'Arunachal Pradesh', N'Arunachal Pradesh', N'IN', N'AR', N'0000076367')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFAE', N'40', N'WF01', N'United Arab Emirates', N'United Arab Emirates', N'AE', N'', N'0000076287')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFAP', N'40', N'WF01', N'Andhra Pradesh', N'WFAP', N'IN', N'', N'0000076288')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFBA', N'40', N'WF01', N'Bavaria', N'Bavaria', N'DE', N'09', N'0000076289')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFBR', N'40', N'WF01', N'Brazil', N'BRAZIL', N'BR', N'', N'0000076290')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFBW', N'40', N'WF01', N'Baden-Wuerttemberg', N'Baden-Wuerttemberg', N'DE', N'08', N'0000076291')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFCH', N'40', N'WF01', N'Chhattisgarh', N'Chhattisgarh', N'IN', N'04', N'0000076292')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFCN', N'40', N'WF01', N'China', N'WFCN', N'CN', N'', N'0000076293')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFDE', N'40', N'WF01', N'Germany', N'Germany', N'DE', N'', N'0000076294')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFDL', N'40', N'WF01', N'Delhi', N'Delhi', N'IN', N'', N'0000076295')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFFI', N'40', N'WF01', N'Finland', N'WF01-Finland', N'FI', N'', N'0000076296')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFGU', N'40', N'WF01', N'Gujarat', N'WFGU', N'IN', N'', N'0000076297')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFHE', N'40', N'WF01', N'Hesse', N'Hesse', N'DE', N'06', N'0000076298')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFHR', N'40', N'WF01', N'Haryana', N'WFHR', N'IN', N'', N'0000076299')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFJH', N'40', N'WF01', N'Jharkhand', N'WFJH', N'IN', N'', N'0000076300')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFKA', N'40', N'WF01', N'Karnataka', N'WFKA', N'IN', N'', N'0000076301')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFLS', N'40', N'WF01', N'Lower Saxony', N'Lower Saxony', N'DE', N'03', N'0000076302')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFMH', N'40', N'WF01', N'Maharashtra', N'WFMH', N'IN', N'', N'0000076303')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFMP', N'40', N'WF01', N'Madhya Pradesh', N'WFMP', N'IN', N'', N'0000076304')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFMW', N'40', N'WF01', N'Mecklenburg-Western Pomerania', N'Mecklenburg-Western Pomerania', N'DE', N'13', N'0000076305')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFNL', N'40', N'WF01', N'Netherlands', N'NETHERLANDS - WF01', N'NL', N'', N'0000076306')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFNW', N'40', N'WF01', N'North Rhine-Westphalia', N'North Rhine-Westphalia', N'DE', N'05', N'0000076307')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFRJ', N'40', N'WF01', N'Rajasthan', N'WFRJ', N'IN', N'', N'0000076308')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFRM', N'40', N'WF01', N'Romania', N'WF01-Romania', N'RO', N'', N'0000076309')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFRP', N'40', N'WF01', N'Rhineland-Palatinate', N'Rhineland-Palatinate', N'DE', N'07', N'0000076310')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFRU', N'40', N'WF01', N'Russia', N'WF01 - Russia', N'RU', N'', N'0000076311')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFSA', N'40', N'WF01', N'Saxony-Anhalt', N'Saxony-Anhalt', N'DE', N'15', N'0000076312')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFSH', N'40', N'WF01', N'Schleswig Holstein', N'Schleswig Holstein', N'DE', N'01', N'0000076313')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFSW', N'40', N'WF01', N'Sweden', N'WF01-Sweden', N'SE', N'', N'0000076314')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFSX', N'40', N'WF01', N'Saxony', N'Saxony', N'DE', N'14', N'0000076315')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFTH', N'40', N'WF01', N'Thuringia', N'Thuringia', N'DE', N'16', N'0000076316')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFTN', N'40', N'WF01', N'Tamil Nadu', N'WFTN', N'IN', N'', N'0000076317')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFTS', N'40', N'WF01', N'Telangana', N'', N'IN', N'36', N'0000076363')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFUK', N'40', N'WF01', N'United Kingdom', N'UNITED KINGDOM - WF01', N'GB', N'', N'0000076318')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFUS', N'40', N'WF01', N'United States of America', N'USA - WF01', N'US', N'', N'0000076319')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WFWB', N'40', N'WF01', N'West Bengal', N'WFWB', N'IN', N'', N'0000076320')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGAE', N'40', N'WG01', N'United Arab Emirates', N'United Arab Emirates', N'AE', N'AE', N'0000076321')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGAN', N'40', N'WG01', N'Andaman Nicobar', N'Andaman Nicobar', N'IN', N'AN', N'0000076368')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGAP', N'40', N'WG01', N'Andhra Pradesh', N'Andhra Pradesh', N'IN', N'', N'0000076322')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGAS', N'40', N'WG01', N'Assam', N'Assam', N'IN', N'AS', N'0000076369')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGBD', N'40', N'WG01', N'Bangladesh', N'Bangladesh', N'BD', N'BD', N'0000076323')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGBH', N'40', N'WG01', N'Bihar', N'Bihar', N'IN', N'BH', N'0000076370')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGBR', N'40', N'WG01', N'Brazil', N'Brazil', N'BR', N'DF', N'0000076324')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGCD', N'40', N'WG01', N'Chandigarh', N'Chandigarh', N'IN', N'', N'0000076325')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGCH', N'40', N'WG01', N'Chhattisgarh', N'Chhattisgarh', N'IN', N'', N'0000076326')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGCN', N'40', N'WG01', N'China', N'China', N'CN', N'010', N'0000076327')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGDD', N'40', N'WG01', N'Daman and Diu', N'Daman and Diu', N'IN', N'DD', N'0000076371')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGDH', N'40', N'WG01', N'Dadra & Nagar Haveli', N'Dadra & Nagar Haveli', N'IN', N'DN', N'0000076372')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGDL', N'40', N'WG01', N'Delhi', N'Delhi', N'IN', N'', N'0000076328')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGG', N'40', N'WG01', N'Ghana', N'Ghana', N'GH', N'', N'0000076373')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGGO', N'40', N'WG01', N'Goa', N'Goa', N'IN', N'', N'0000076329')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGGU', N'40', N'WG01', N'Gujarat', N'Gujarat', N'IN', N'', N'0000076330')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGHK', N'40', N'WG01', N'Hong Kong', N'Hong Kong', N'HK', N'HK', N'0000076331')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGHP', N'40', N'WG01', N'Himachal Pradesh', N'Himachal Pradesh', N'IN', N'', N'0000076332')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGHR', N'40', N'WG01', N'Haryana', N'Haryana', N'IN', N'', N'0000076333')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGID', N'40', N'WG01', N'Indonesia', N'Indonesia', N'ID', N'ID', N'0000076334')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGJD', N'40', N'WG01', N'Jharkhand', N'Jharkhand', N'IN', N'JH', N'0000076374')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGJK', N'40', N'WG01', N'Jammu and Kashmir', N'Jammu and Kashmir', N'IN', N'JK', N'0000076375')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGJP', N'40', N'WG01', N'Japan', N'Japan', N'JP', N'13', N'0000076335')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGKA', N'40', N'WG01', N'Karnataka', N'Karnataka', N'IN', N'', N'0000076336')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGKL', N'40', N'WG01', N'Kerala', N'Kerala', N'IN', N'', N'0000076337')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGLK', N'40', N'WG01', N'Srilanka', N'Srilanka', N'LK', N'LK', N'0000076338')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGMG', N'40', N'WG01', N'Meghalaya', N'Meghalaya', N'IN', N'ME', N'0000076376')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGMH', N'40', N'WG01', N'Maharashtra', N'Maharashtra', N'IN', N'', N'0000076339')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGMN', N'40', N'WG01', N'Manipur', N'Manipur', N'IN', N'MN', N'0000076377')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGMP', N'40', N'WG01', N'Madhya Pradesh', N'Madhya Pradesh', N'IN', N'', N'0000076340')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGMY', N'40', N'WG01', N'Malaysia', N'Malaysia', N'MY', N'MY', N'0000076341')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGMZ', N'40', N'WG01', N'Mizoram', N'Mizoram', N'IN', N'MI', N'0000076378')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGNG', N'40', N'WG01', N'Nigeria', N'Nigeria', N'NG', N'AB', N'0000076342')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGNP', N'40', N'WG01', N'Nepal', N'Nepal', N'NP', N'NEP', N'0000076343')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGOR', N'40', N'WG01', N'Orissa', N'Orissa', N'IN', N'', N'0000076344')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGPB', N'40', N'WG01', N'Punjab', N'Punjab', N'IN', N'PB', N'0000076379')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGPH', N'40', N'WG01', N'Philippines', N'Philippines', N'PH', N'', N'0000076380')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGPY', N'40', N'WG01', N'Pondicherry', N'Pondicherry', N'IN', N'PY', N'0000076381')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGRJ', N'40', N'WG01', N'Rajasthan', N'Rajasthan', N'IN', N'', N'0000076345')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGRO', N'40', N'WG01', N'Romania', N'Romania', N'RO', N'BUC', N'0000076346')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGSA', N'40', N'WG01', N'Saudi Arabia', N'Saudi Arabia', N'SA', N'01', N'0000076347')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGSE', N'40', N'WG01', N'Sweden', N'Sweden', N'SE', N'019', N'0000076348')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGSG', N'40', N'WG01', N'Singapore', N'Singapore', N'SG', N'SG', N'0000076349')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGSK', N'40', N'WG01', N'Sikkim', N'Sikkim', N'IN', N'SK', N'0000076382')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGTH', N'40', N'WG01', N'Thailand', N'Thailand', N'TH', N'C', N'0000076350')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGTN', N'40', N'WG01', N'Tamil Nadu', N'Tamil Nadu', N'IN', N'', N'0000076351')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGTR', N'40', N'WG01', N'Tripura', N'Tripura', N'IN', N'TR', N'0000076383')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGTS', N'40', N'WG01', N'Telangana', N'Telangana', N'IN', N'TEL', N'0000076352')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGUK', N'40', N'WG01', N'UNITED KINGDOM', N'UNITED KINGDOM', N'GB', N'', N'0000076353')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGUP', N'40', N'WG01', N'Uttar Pradesh', N'Uttar Pradesh', N'IN', N'', N'0000076354')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGUT', N'40', N'WG01', N'Uttaranchal', N'Uttaranchal', N'IN', N'', N'0000076355')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGVN', N'40', N'WG01', N'Vietnam', N'Vietnam', N'VN', N'', N'0000076356')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGWA', N'40', N'WG01', N'Washington', N'Washington', N'US', N'WA', N'0000076357')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGWB', N'40', N'WG01', N'West Bengal', N'West Bengal', N'IN', N'', N'0000076358')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WGZA', N'40', N'WG01', N'South Africa', N'South Africa', N'ZA', N'KW', N'0006580715')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WLAK', N'40', N'WG01', N'Lakshadweep', N'Lakshadweep', N'IN', N'LD', N'0000076384')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WNG', N'40', N'WG01', N'Nagaland', N'Nagaland', N'IN', N'NL', N'0000076385')
GO
INSERT [dbo].[dest_PersonalArea_M] ([PA], [CGrpg], [CoCd], [Personnel Area Text], [Name 2], [Ctr], [Rg], [Address]) VALUES (N'WUK', N'40', N'WG01', N'Uttarakhand', N'Uttarakhand', N'IN', N'UT', N'0000076386')
GO

INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'01', N'WIPRO')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'03', N'USA PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'07', N'GERMANY PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'08', N'SWEDEN PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'09', N'FINLAND PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'10', N'NETHERLAND PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'17', N'CHINA PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'19', N'UAE PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'33', N'BRAZIL PSCL  AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'35', N'ROMANIA PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'61', N'RUSSIA PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'28', N'VIETNAM PSCL AREA')
GO
INSERT [dbo].dest_PayScaleArea_M ([PayScaleArea], [Description]) VALUES (N'WG', N'WCCLG')
---------------------------------------------------


GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'/TPA', N'31.12.9999', N'01.01.1901', N'Total Package', N'TotPack')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1ADD', N'31.12.9999', N'01.01.1900', N'Additional Allowance', N'Ad Allow')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1BAS', N'31.12.9999', N'01.01.1900', N'Basic Salary', N'Basic')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1CAR', N'31.12.9999', N'01.01.1900', N'Care Lease', N'CAR')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1CTC', N'31.12.9999', N'01.01.1900', N'WT COST TO COMPANY', N'WT CTC')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1ESI', N'31.12.9999', N'01.01.1900', N'ESI', N'ESI')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1FEP', N'31.12.9999', N'01.01.1900', N'Wipro Benefits Plan', N'WBP')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1FNE', N'31.12.9999', N'01.01.1900', N'Furniture and Equipment', N'FNE')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1MED', N'31.12.9999', N'01.01.1900', N'Medical', N'MEDICAL')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1QPL', N'31.12.9999', N'01.01.1900', N'Variable Pay', N'QPLC')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1STP', N'31.12.9999', N'01.01.1900', N'Stipend', N'STIPEND')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'2BAS', N'31.12.9999', N'01.01.1900', N'Base Salary', N'2BASE')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7BAS', N'31.12.9999', N'01.01.1900', N'WIN BASIC 30%', N'WIN BASI')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7BON', N'31.12.9999', N'01.01.1900', N'Bonus', N'Bonus')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7CTC', N'31.12.9999', N'01.01.1900', N'WF COST TO COMPANY', N'WF CTC')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7ESI', N'31.12.9999', N'01.01.1900', N'WIN ESI 4.75%', N'WIN ESI')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7GT2', N'31.12.9999', N'01.01.1900', N'WIN GRATUITY 5.31%', N'WIN GRAT')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7MED', N'31.12.9999', N'01.01.1900', N'Medical', N'Medical')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7PFV', N'31.12.9999', N'01.01.1900', N'Provident Fund', N'PF')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7STP', N'31.12.9999', N'01.01.1900', N'WIN STIPEND', N'WIN STP')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7TRA', N'31.12.9999', N'01.01.1900', N'Trainee Allowance', N'TRNALLW')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M200', N'31.12.9999', N'01.01.1998', N'Uniform Allowance', N'Unf Alln')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M210', N'31.12.9999', N'01.01.1998', N'LTA amount', N'LTA Amt')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M211', N'31.12.9999', N'01.01.1998', N'Medical amount', N'Med Amt')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M212', N'31.12.9999', N'01.01.1998', N'Medical insurance', N'Med Ins')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M213', N'31.12.9999', N'01.01.1998', N'HFS eligibility', N'Hfs Elg')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M214', N'31.12.9999', N'01.01.1998', N'SFS eligibility', N'Sfs Elg')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M215', N'31.12.9999', N'01.01.1998', N'Car maintenance reimburse', N'CarMReim')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M220', N'31.03.2015', N'01.01.1998', N'Conveyance Allowance', N'Cnv Amt')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M220', N'31.12.9999', N'01.04.2015', N'Conveyance Allowance', N'Cnv Amt')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M222', N'31.12.9999', N'01.01.1998', N'Act Expenditure Amt OYCS', N'Expn Amt')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M230', N'31.03.2015', N'01.01.1998', N'House Rent Allowance', N'HRA Alln')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M230', N'31.12.9999', N'01.04.2015', N'House Rent Allowance', N'HRA Alln')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M231', N'31.12.9999', N'01.01.1998', N'Company leased accom', N'CLA Alln')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M232', N'31.12.9999', N'01.01.1998', N'Company owned accom', N'CLA Alln')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M260', N'31.12.9999', N'01.01.1998', N'Children Education Allow.', N'ChEduAll')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M270', N'31.12.9999', N'01.01.1998', N'Children Hostel Allowance', N'ChHosAll')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M281', N'31.12.9999', N'01.01.1998', N'Stitching charges', N'Stitch')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M282', N'31.12.9999', N'01.01.1998', N'Monthly Reimbursement', N'MthReim')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M283', N'31.12.9999', N'01.01.1998', N'Calendar year reimb.', N'CalYr')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'M284', N'31.12.9999', N'01.01.1998', N'Yearly reimbursement - F', N'YearlyF')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'MB10', N'31.03.2015', N'01.01.1998', N'Basic Salary', N'Basic')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'MB10', N'31.12.9999', N'01.04.2015', N'Basic Salary', N'Basic')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'MLE0', N'31.12.9999', N'01.01.1998', N'Leave Encashment', N'LENCASH')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'MVR0', N'31.12.9999', N'01.01.1998', N'Voluntary Retirement', N'VOLRETS')
GO
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'Z045', N'31.12.9999', N'01.01.1998', N'GB: Oncall/stand by', N'GB:OnCal')



INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'1STP', N'31.12.9999', N'01.01.1900', N'Stipend', N'STIPEND')
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'2BAS', N'31.12.9999', N'01.01.1900', N'Base Salary', N'2BASE')
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'7STP', N'31.12.9999', N'01.01.1900', N'WIN STIPEND', N'WIN STP')
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'8ADL', N'31.12.9999', N'01.01.1900', N'WCCLG Additional Allow', N'CCLG ADL')
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'8BAS', N'31.12.9999', N'01.01.1900', N'WCCLG BASIC', N'CCLG BAS')
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'8CPL', N'31.12.9999', N'01.01.1900', N'WCCLG CPLB', N'CCLG CPL')
INSERT [dbo].[dest_WageType_M] ([CountryGroup], [WageType], [EndDate], [StartDate], [Longtext], [WTShortText]) VALUES (40, N'8CTC', N'31.12.9999', N'01.01.1900', N'WCCLG Cost to Company', N'CCLG CTC')


INSERT [dbo].[dest_BankDetailsType_M] ([BankDetailsType], [Descrption]) VALUES (0, N'Home Bank')
GO
INSERT [dbo].[dest_BankDetailsType_M] ([BankDetailsType], [Descrption]) VALUES (1, N'Host Bank')
GO
INSERT [dbo].[dest_BankDetailsType_M] ([BankDetailsType], [Descrption]) VALUES (2, N'Travel Expenses')
GO

INSERT [dbo].[dest_PaymentMethods_M] ([PaymentMethod], [Description]) VALUES (N'Z', N'HR Payment method ')


INSERT [dbo].[dest_Child_M] ([Id], [Description]) VALUES (N'01', N'Kid1')
GO
INSERT [dbo].[dest_Child_M] ([Id], [Description]) VALUES (N'02', N'Kid2')
GO
INSERT [dbo].[dest_Child_M] ([Id], [Description]) VALUES (N'03', N'Kid3')



INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (1, N'Spouse')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (10, N'Divorced spouse')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (11, N'Father')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (12, N'Mother')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (13, N'Brother')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (14, N'Sister')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (15, N'Registered Partner')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (18, N'Father-in-Law')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (19, N'Mother-in-Law')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (2, N'Child')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (3, N'Legal guardian')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (4, N'Testator')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (5, N'Guardian')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (6, N'Stepchild')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (7, N'Emergency contact')
GO
INSERT [dbo].[dest_TypeOfFamilyRecord_M] ([TypeOfFamilyRecord], [Description]) VALUES (8, N'Related persons')



INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2008-01-01T00:00:00.000' AS DateTime), N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'LP01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-04-15T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PSML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-04-15T00:00:00.000' AS DateTime), N'Post Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave Half-Day')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'USPO', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'PTO - Paid Time-Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'2007-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2008-01-01T00:00:00.000' AS DateTime), N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'CO01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Compensatory Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'COOM', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Extended Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Additional MaternityLeave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'IJ01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Industrial Injury Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'LP01', CAST(N'2013-04-14T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'LP01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-04-15T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PRML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Pre Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PSML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Post Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2014-01-01T00:00:00.000' AS DateTime), N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2018-10-01T00:00:00.000' AS DateTime), N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'2007-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2008-01-01T00:00:00.000' AS DateTime), N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'CO01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Compensatory Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Extended Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Additional MaternityLeave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'IJ01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Industrial Injury Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'LP01', CAST(N'2013-04-14T00:00:00.000' AS DateTime), NULL, N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'LP01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-04-15T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PRML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Pre Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PSML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Post Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2014-01-01T00:00:00.000' AS DateTime), N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2018-10-01T00:00:00.000' AS DateTime), N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Approved Paid Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half-Day Paid Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Loss Of Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'COOM', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Compensatory Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Election/Compulsory Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Emergency Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half-Day Emergency Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'IJ01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Industrial Injury Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave Half-Day')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Absent')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'USLP', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'USPO', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2009-01-01T00:00:00.000' AS DateTime), N'PTO - Paid Time-Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'2007-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2008-01-01T00:00:00.000' AS DateTime), N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2008-01-01T00:00:00.000' AS DateTime), N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Vacation Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Half Day Vacation Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Approved Leav Without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'COOM', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Compensatory Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half Day Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'UnapprovedLeav WithoutPay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Parental Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Vacation Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Half Day Vacation Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Approved Leav Without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'COOM', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Compensatory Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half Day Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'UnapprovedLeav WithoutPay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Annual leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Parental/Child Care Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Annual leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half day Annual leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Loss Of Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half day Sick leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'LP01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2015-01-01T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PRML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Pre Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PSML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Post Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-01-01T00:00:00.000' AS DateTime), N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFAN', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFEM', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Extended Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFIJ', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Industrial Injury Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFLP', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-01-01T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFMS', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Miscarriage Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PRML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Pre Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PSML', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Post Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-01-01T00:00:00.000' AS DateTime), N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFAN', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFEM', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Extended Maternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFIJ', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Industrial Injury Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFLP', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2013-01-01T00:00:00.000' AS DateTime), N'Leave without Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'WFMS', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Miscarriage Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Vacation Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Emergency/Force Maj Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Parental Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Annual leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Loss Of Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Parental/Child Care Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Absent')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Annual Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Parental Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Vacation Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Emergency/Force Maj Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PT01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Parental Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9998', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Half-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'9999', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'1980-01-01T00:00:00.000' AS DateTime), N'Full-Day Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), CAST(N'2000-01-01T00:00:00.000' AS DateTime), N'Approved Paid Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'AN02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half-Day Paid Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'APLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Loss Of Pay')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'COOM', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Compensatory Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Election/Compulsory Off')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Emergency Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'EM02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Half-Day Emergency Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'IJ01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Industrial Injury Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'PTL1', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Paternity Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'SL02', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Sick Leave Half-Day')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'TR01', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Transfer Leave')
GO
INSERT [dbo].[dest_Leaves_Type_M] ([AORAType], [EndDate], [StartDate], [AttORABSTypeText]) VALUES (N'UNLW', CAST(N'9999-12-31T00:00:00.000' AS DateTime), NULL, N'Absent')
GO



INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'01', N'CUSTOMER BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'02', N'WI - WIRE')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'03', N'HOT SKILLS-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'04', N'CASH REWARD-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'05', N'AMS RETENTION BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'06', N'NERVEWIRE RETENTION BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'07', N'WT - RETENTION BONUS CAMPUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'08', N'WT - DEFERRED CASH')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'09', N'LEHMAN RETENTION BONUS - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'10', N'INNOVATION THEMES INC PRG - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'11', N'JPMC & BAFTA BONUS PAYMNT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'12', N'SONY-ON-CALL PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'13', N'LEHMAN BROS SHIFT PAYMENT-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'14', N'NORTEL OT PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'15', N'TRANSCODER T&I 24X7 PAYMENT-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'16', N'NIGHT SHIFT ALLOW PAYMENT- WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'17', N'UPROMISE WEEKEND WRKG PAYMT-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'18', N'GCR-CI TEAM BONUS PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'19', N'SONY HOLIDAY SUPP COMPEN - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'20', N'BP PROJECT-SHIFT ALLOW-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'21', N'CISCO AWARD-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'22', N'EMC SHIFT PAYMENT-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'23', N'ERICSSON ODC: 24X7 COMPEN-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'24', N'GILLETTE SHIFT ALLOWANCE -WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'25', N'GM GIF 24X365 OPNS SUP NT SF P')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'26', N'LEHMAN BROS - ON CALL ALLOW-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'27', N'MICROSOFT - HARDSHIP ALLOW-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'28', N'NCR - BONUS AWARD-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'29', N'SCA PRDN SUPP-ODD SFT ALLW-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'30', N'OT BILLING FR FALCON TEAM-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'31', N'SEAGATE 24X7 SIB SUPP - SFT AL')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'32', N'QUEST PROJECT - SHIFT ALLOW-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'33', N'SONY - NIGHT SHIFT ALLOW-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'34', N'WHAEUSER PRODN SUPP - BONUS-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'35', N'UPROMISE WEEKEND WRKG PAYMT-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'36', N'AWS SHIFT ALLOWANCE - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'37', N'SPECIAL BONUS - SLAB 1 - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'38', N'WT - SPECIAL BONUS - SLAB 2')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'39', N'WT - SHIFT PAYMENT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'40', N'WT - ODD HOURS ALLOWANCE')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'41', N'24/7 ALLOWANCE - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'42', N'NORTEL UPFRONT PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'43', N'AWS SHIFT ALLOWANCE - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'44', N'MS SIEBEL USER SUP - SHIFT -WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'45', N'POPAXIS 24X5 PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'46', N'CISCO 24X7X365 SUPP COMP - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'47', N'COMMON TIME PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'48', N'EMERSON ITSS SHIFT PAYT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'49', N'HARDSHIP ALLOWANCE - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'50', N'WT - INVENTION DISCLOSURE PAYT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'51', N'MAXXAN SHIFT PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'52', N'PRODUCTION SUPP SHIFT PAY WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'53', N'WCEP 2003 - LEHMAN BONUS -WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'54', N'THAMES WATER ALLOWANCE - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'55', N'ACCENTURE SHIFT ALLOWANCE - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'56', N'ADDL HOURS WORKED PAYMT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'57', N'PUTNAM - SHIFT PAYMENT - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'58', N'INCENTIVE SCHEME - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'59', N'AACP 2004 - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'60', N'WT - HOLIDAY WORKING')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'61', N'ON TIME BONUS - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'62', N'CLIENT APPRECIATION PMT-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'63', N'WT - OVERTIME WORKING PAYMENT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'64', N'WT - EXCEPTIONAL EFFORTS REWD')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'65', N'LEHMAN HOT SKILLS - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'66', N'WIPRO HOT SKILLS PROGRAM - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'67', N'WIRE PROGRAM - CORPORATE')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'68', N'CUSTOMER BONUS AWARD - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'69', N'CUSTOMER LONG SERVICE AWARD-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'70', N'STANDBY ALLOWANCE - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'71', N'BONUS AWARD - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'72', N'ISU Onsite Special Bonus - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'73', N'AACP 2006 - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'74', N'Certificate Prgm Cost Reimb-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'75', N'Fresh traveler Spl Bonus Apr06')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'76', N'EAS Spl Bonus Sep06 SLAB1�WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'77', N'EAS Spl Bonus Sep06 SLAB2�WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'78', N'E-biz Spl Bonus Oct06 SLAB1�WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'79', N'E-biz Spl Bonus Oct06 SLAB2�WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'80', N'JOINING BONUS-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'81', N'WIRE-Company Retention-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'82', N'WIRE-Role Retention-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'83', N'CAB-Company Retention-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'84', N'CAB-Role Retention-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'85', N'JOINING CAB-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'86', N'Notice Pay - WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'87', N'Performance Bonus 2006 TRB-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'88', N'SATURDAY WORKING - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'89', N'Local Onsite allowance-WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'90', N'WT - JOINING BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'91', N'WT - Large Deal Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'92', N'WT - ON / STAND BY/ CALL OUT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'93', N'WT - LOCAL ONSITE')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'94', N'TRB DEFFERED PERF. PAY - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'95', N'WT - WIRE')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'96', N'BONUS INCL AT TIME OF TRF-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'97', N'WT - EXTRA REWARDS PROGRAMME')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'98', N'RET.BONUS-CUSTOMER SPON. - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'99', N'RET.BONUS-WIPRO SPONSORDF - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A1', N'RET.BONUS-PARTLY WIPRO SP - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A2', N'REWARD-ONETIME CUST. SPON - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A3', N'REWARD-ONETIME WIPRO SPON - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A4', N'WT REWARD-ONETIMEPARTLY WIP SP')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A5', N'WT - RECT. ONE-TIME BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A6', N'WT-RECT DEFERRED BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A7', N'WT-WIN - WIRE')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A8', N'WI - Shift Allowance')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'A9', N'SPECIAL PROJECT BONUS - WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B1', N'WT - GUARANTEED INCENTIVES')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B2', N'WT - JOINING BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B3', N'RETENTION BONUS-SBI BOND-WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B4', N'Performance Bonus-WBPO')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B5', N'WT-Retention Bonus-WBPO')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B6', N'WI - Incentive Scheme')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B7', N'WI - Employee Referal Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B8', N'Notice Period Pay - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'B9', N'Bond Repayment - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C1', N'PERF. BASED RETENTION BONUS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C2', N'Relocation Expense - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C3', N'Critical Assignment Bonus-WBPO')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C4', N'Travel Expenses')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C5', N'Hotel Expenses')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C6', N'Notice Pay')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C7', N'Bond re-payment')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C8', N'WT - Others Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'C9', N'WT - Book Allowance')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D1', N'WI - Book Allowance')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D2', N'WT - Customer Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D3', N'WT-Niche Skill Retention Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D4', N'Training Bonus - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D5', N'WT-Critical Resource Retention')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D6', N'WT - Milestone Based Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D8', N'WT - New Hire Joining Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'D9', N'New Hire Relocation Bonus - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E1', N'WT - Wipro KTC Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E2', N'Employee Referral Bo - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E3', N'WI - Customer Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E4', N'WI - Niche Skill Retention Bon')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E5', N'WI - Training Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E6', N'WI-Critical Resource Retention')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E7', N'WI - Milestone Based Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E8', N'WI - ON CALL ALLOWANCE')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'E9', N'WI - New Hire Joining Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F1', N'WI - New Hire Relocation Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F2', N'WI - Wipro KTC Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F3', N'Employee Referral Bonus - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F4', N'WI - Others Bonus�������������')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F5', N'Signing Bonus - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F6', N'DEFERRED CASH - WI')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F7', N'WT - Bonus Pay - WBPO')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F8', N'Account Linked Bonus - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'F9', N'WI-Niche Skill Retention Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G1', N'WT - Account Specific')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G2', N'WI - Account Specific')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G3', N'WT - BCS')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G4', N'WT - Productivity')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G5', N'WT - GTR Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G6', N'WT - DMTS Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G7', N'WT - Workforce Transitions')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G8', N'WT -Sales Proactive Deal Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'G9', N'WT - Sales Empanelment Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H1', N'WT - Sales Discretionary')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H2', N'WT - Intra city Hardship Bonus')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H3', N'Transfer Hardship Bonus -WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H4', N'Milestone Bonus with Merg - WT')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H5', N'WI - Milestone Bonus with Merg')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H6', N'Chennai Relief Fund - CCLG')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H7', N'WERP - CCLG')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H8', N'Engagement bonus - CCLG')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'H9', N'WT - NIC-Special Payment')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'I1', N'WT-Germany School Fee -One-Tim')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'I2', N'WT - TopGear')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'I3', N'WT - Mileage')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'I4', N'WT - D-120 payment')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'I6', N'Bonus-only Merge in Sal�No pay')
GO
INSERT [dbo].[dest_AdditionalPayments_M] ([ReasonForChangingMasterData], [Description]) VALUES (N'RT', N'Romania Tax Bonus')
GO










