SELECT  FORMAT(a.[Start Date],'yyyy-MM-dd')  AS [EventDate],
		(SELECT ROW_NUMBER() OVER(PARTITION BY wccp.[Employee_Num] ORDER BY a.[Action] ASC)) AS [SeqNumber],
		wccp.[Employee_Num] AS [UserId], a.[Reason For Action] AS [EventReason], 
		wccp.PositionCode AS [Position], wccp.CompanyCode AS [Company], wccp.CompanyName, wccp.BusinessUnitCode AS [BusinessUnitCode],  wccp.BusinessUnit AS [BusinessUnit],
		wccp.DivisionCode AS [DivisionCode], wccp.Division AS [Division], 
		wccp.DepartmentCode AS [DepartmentCode], wccp.Department AS [Department], 
		wccp.PA_PSA_Name AS [PaPsa], wccp.PA_PSA_Code AS [PaPsaCode],
		' ' AS [GeoZoneOrRegion], '' AS [TimeZone], wccp.Cost_Centre AS [CostCenter], Supervisor_ID AS [SupervisorId], Supervisor_Name AS [Supervisor], 
		wccp.Job_Role_Code AS [JobClassification], wccp.[Role] AS [JobClassificationName], --ass.Job AS [JobClassification],
		wccp.DesignationCode AS [DesignationCode], wccp.Designation_text AS [Designation], wccp.EmployeeGroup_Code AS [EmployeeGroupCode], 
		wccp.EmployeeGroup_Description AS [EmployeeGroup], wccp.EmpSubGrpCode AS [EmployeeSubGroupCode],
		wccp.EmployeeSubGrp AS [EmployeeSubGroup], ' ' AS [CompetitionClause], 
		--CASE 
		--		WHEN ass.Employee_Group = '4' and a.[action] = '1'
		--			THEN FORMAT (DATEADD(year, 1, a.[Start Date]), 'yyyy-MM-dd')
		--		WHEN 
		--			ass.Employee_Group = '5' and a.[action] = '5' --Allow only India 
		--			THEN FORMAT (a.[Start Date], 'yyyy-MM-dd')
		--		ELSE
		--			' ' 
		--END 
		' ' AS [ProbationaryPeriodEndDate],
		' ' AS [PayScaleType], ' ' AS [PayScaleArea],
		' ' AS [PayScaleGroup], ' ' AS [PayScaleLevel], --ass.Position, ' ', aq.[Acquired_Company_Details],	' ' , ' ' , 
		' ' AS [EmployeeNoticePeriod], ' ' AS  [ESI Applicable], ' ' AS [EmployeeWorkLocation] , ' '  AS [OnSiteLocation] , ' ' AS [LocationRegion] , ' '  AS [Classification], 
		' ' AS [HolidayCalendar], ' ' AS [WorkSchedule], ' 'AS [TimeProfile], ' ' AS [TimeRecordingProfile],' ' AS [TimeRecordingAdmissibility], ' ' AS [TimeRecordingVariant]
FROM [dbo].[src_Active Profile Mapping_28.10.21_to_populate_position] wccp
--INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' 
INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = wccp.[Employee_Num] --and a.[End Date] = '12/31/9999' 
--LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')
--WHERE  a.[Action] IN('01', '02', '03', '12', '53', '72', '99', 'BK', 'ZX', 'GT') 

