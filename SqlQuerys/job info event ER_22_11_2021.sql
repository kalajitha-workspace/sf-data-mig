
SELECT  FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		(SELECT ROW_NUMBER() OVER(PARTITION BY wccp.[Employee_Num] ORDER BY a.[Action] ASC)) AS [SeqNumber],
		wccp.Employee_Num AS [UserId], 
		wccp.PositionCode,
		am.NameOfActionType AS [LegacyEvent],
		am.[Action] AS [LegacyEventCode],
		arm.NameOfReasonForAction AS [LegacyEventReason], 
		a.[Reason For Action] AS [LegacyEventReasonCode], 
		wccp.CompanyCode AS [Company], 
		wccp.BusinessUnitCode AS [BusinessUnit], 
		wccp.DivisionCode  AS [Division], 
		wccp.[DepartmentCode] AS [Department],
		wccp.PA_PSA_Code AS [PaPsa],
		wccp.Cost_Centre AS [CostCenter],
		wccp.Supervisor_ID AS [Manager],
		wccp.Job_Role_Code as JobRole,
		wccp.DesignationCode  AS Designation,
		CASE 
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%permanent%'
				THEN 'Permanent'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%probationer%'
				THEN 'Probationer'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%contract%'
				THEN 'Contract'
			--WHEN lower(wccp.EmployeeGroup_Description) LIKE '%interns%'
			--	THEN 'Interns'
			END AS [EmployeeType], 
		wccp.EmployeeGroup_Description AS [EmployeeGroup], 
		--a.[action] AS [action],
		wccp.EmployeeSubGrp AS [Grade], 	
		CASE 
				-- Action: 01	Hiring
				-- Assignment : 4	Probationer
				-- A.startdate( joining date) + 1 year -> ProbationaryPeriodEndDate
				WHEN ass.Employee_Group = '4' and a.[action] = '01'
					THEN FORMAT (DATEADD(year, 1, a.[Start Date]), 'yyyy-MM-dd')
				WHEN 
				-- Assignment : 5	Permanent
				-- Action : 05	Confirmation
				-- A.startdate(Confirmation date as) -> ProbationaryPeriodEndDate
					ass.Employee_Group = '5' and a.[action] = '5' --As of now the data given is only India  employees
					THEN FORMAT (a.[Start Date], 'yyyy-MM-dd')
				ELSE
					' ' 
		END AS [ProbationaryPeriodEndDate],
		'' AS [ContractEndDate],
		CASE 
			-- Assignment : 4	Probationer
			WHEN ass.Employee_Group = '4' 
				THEN '1 month'
			-- Assignment : 5	Permanent
			WHEN ass.Employee_Group = '5' 
				THEN					
					CASE 
						-- ESG Legacy(old) Code : SSG & A 
						WHEN wccp.EmpSubGrpCode IN ('EU','ET','ES','ER','EQ')
							THEN '4 months'
						-- ESG Legacy(old) Code : B, B1 , C , D, D1 , E 
						WHEN wccp.EmpSubGrpCode IN ('FA','EZ','EY','EX','EW','EV') 
							THEN '3 months'
						-- ESG Legacy(old) Code : 	F1, F2 
						WHEN wccp.EmpSubGrpCode IN ('FC','FB') 
							THEN '2 months'
						-- ESG Legacy(old) Code : G,G1,G2,H 
						WHEN wccp.EmpSubGrpCode IN ('FG','FF','FE','FD') 
							THEN '1 month'
					END		 
		END AS [EmployeeNoticePeriod],
		'' AS [Notes],
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], a.[Changed by] as [ChangedBy]
FROM [dbo].[src_active_17_11_2021] wccp
--INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' 
INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = wccp.[Employee_Num] and a.[End Date] = '12/31/9999'-- (only 999 we need probation perios end date)

--Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number 
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
--INNER Join [dbo].[src_employees_tagging_report] etr on etr.[EMP_NO] = a.[Personnel number] 
--LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
Inner Join [dbo].[src_Assignment] ass on wccp.[Employee_Num] = ass.Personnel_number and ass.End_Date = '12/31/9999' 
--WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')