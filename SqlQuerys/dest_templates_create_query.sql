USE [SF_Datamigration]
GO
/****** Object:  Table [dbo].[dest_Basic_User_Info]    Script Date: 12/6/2021 11:32:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Basic_User_Info](
	[Status] [nvarchar](50) NULL,
	[UserId] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[ReportingOfficer] [int] NOT NULL,
	[HrManager] [int] NOT NULL,
	[Gender] [nvarchar](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Biographical_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Biographical_Info](
	[UserId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[DateOfBirth] [datetime] NOT NULL,
	[CountryOrRegionOfBirth] [nvarchar](50) NULL,
	[CountryOfOrigin] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Comp_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Comp_Info](
	[UserId] [int] NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[EventReason] [nvarchar](50) NOT NULL,
	[PayGroup] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_CSF_Addresses]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_CSF_Addresses](
	[PersonIdExternal] [int] NULL,
	[EventDate] [datetime] NULL,
	[AddressType] [nvarchar](510) NULL,
	[CountryRegion] [nvarchar](50) NULL,
	[HouseNumber] [nvarchar](50) NULL,
	[Street] [nvarchar](100) NULL,
	[ExtraAddressLine] [nvarchar](50) NULL,
	[Pin] [nvarchar](12) NULL,
	[City] [nvarchar](50) NULL,
	[District] [nvarchar](50) NULL,
	[State] [nvarchar](3) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Dependent_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Dependent_Info](
	[EventDate] [datetime] NOT NULL,
	[PersonIdExternal] [int] NOT NULL,
	[Relationship] [nvarchar](50) NOT NULL,
	[Accompanying] [nvarchar](50) NULL,
	[CopyAddressFromEmployee] [nvarchar](50) NULL,
	[IsBeneficiary] [nvarchar](50) NULL,
	[RelatedPersonIdExternal] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[MiddleName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Gender] [nvarchar](1) NOT NULL,
	[DateOfBirth] [datetime] NULL,
	[CountryOfBirth] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[NationalIDCardType] [nvarchar](50) NOT NULL,
	[NationalID] [nvarchar](50) NOT NULL,
	[IsPrimary] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Email_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Email_Info](
	[PersonIdExternal] [int] NOT NULL,
	[EmailType] [nvarchar](50) NOT NULL,
	[EmailAddress] [nvarchar](50) NOT NULL,
	[IsPrimary] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Emergency_Contact]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Emergency_Contact](
	[PersonIdExternal] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Relationship] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](12) NULL,
	[EmergencyContact] [bit] NULL,
	[Primary] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Employment_Info-At_Hire]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Employment_Info-At_Hire](
	[PersonIdExternal] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[HireDate] [datetime] NOT NULL,
	[GroupHireDate] [datetime] NOT NULL,
	[CardNumber] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Employment_Info-At_Term]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Employment_Info-At_Term](
	[UserId] [int] NOT NULL,
	[PersonIdExternal] [int] NOT NULL,
	[SeparationDate] [datetime] NULL,
	[ReasonForExit] [nvarchar](50) NULL,
	[ReasonForExit2] [nvarchar](50) NULL,
	[Notes] [nvarchar](255) NULL,
	[PayrollEndDate] [datetime] NULL,
	[LastDateWorked] [datetime] NULL,
	[OkToRehire] [int] NULL,
	[TerminationReason] [nvarchar](50) NOT NULL,
	[RegretTermination] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Job_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Job_Info](
	[EventDate] [datetime] NOT NULL,
	[SeqNumber] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[EventReason] [nvarchar](50) NOT NULL,
	[Position] [int] NULL,
	[Company] [nvarchar](50) NOT NULL,
	[BusinessUnit] [nvarchar](50) NOT NULL,
	[Division] [nvarchar](50) NULL,
	[Department] [nvarchar](50) NULL,
	[PaPsa] [nvarchar](50) NULL,
	[GeoZoneOrRegion] [nvarchar](50) NULL,
	[TimeZone] [nvarchar](50) NULL,
	[CostCenter] [nvarchar](50) NULL,
	[Supervisor] [nvarchar](50) NOT NULL,
	[JobClassification] [int] NOT NULL,
	[Designation] [nvarchar](50) NULL,
	[EmployeeGroup] [int] NULL,
	[EmployeeSubGroup] [nvarchar](10) NOT NULL,
	[CompetitionClause] [nvarchar](50) NOT NULL,
	[ProbationaryPeriodEndDate] [datetime] NULL,
	[PayScaleType] [int] NULL,
	[PayScaleArea] [int] NULL,
	[PayScaleGroup] [nvarchar](50) NULL,
	[PayScaleLevel] [int] NULL,
	[EmployeeNoticePeriod] [nvarchar](50) NOT NULL,
	[EsiApplicable] [nvarchar](50) NULL,
	[EmployeeWorkLocation] [nvarchar](50) NULL,
	[OnSiteLocation] [nvarchar](50) NULL,
	[LocationRegion] [nvarchar](50) NULL,
	[Classification] [nvarchar](50) NULL,
	[HolidayCalendar] [nvarchar](50) NULL,
	[WorkSchedule] [nvarchar](50) NULL,
	[TimeProfile] [nvarchar](50) NULL,
	[TimeRecordingProfile] [nvarchar](50) NULL,
	[TimeRecordingAdmissibility] [nvarchar](50) NULL,
	[TimeRecordingVariant] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Job_Info_v2]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Job_Info_v2](
	[EventDate] [date] NOT NULL,
	[SeqNumber] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Event] [nvarchar](50) NOT NULL,
	[EventReason] [nvarchar](50) NOT NULL,
	[CompanyCode] [nvarchar](50) NOT NULL,
	[BusinessUnit] [nvarchar](50) NOT NULL,
	[Division] [nvarchar](50) NULL,
	[Department] [nvarchar](50) NULL,
	[PaPsa] [nvarchar](50) NULL,
	[CostCenter] [nvarchar](50) NULL,
	[Manager] [int] NOT NULL,
	[EmployeeType] [nvarchar](50) NULL,
	[EmployeeGroup] [nvarchar](50) NULL,
	[Grade] [nvarchar](50) NOT NULL,
	[ProbationaryPeriodEndDate] [date] NULL,
	[ContractEndDate] [date] NULL,
	[EmployeeNoticePeriod] [nvarchar](50) NOT NULL,
	[Notes] [nvarchar](255) NULL,
	[ChangedDate] [nvarchar](50) NULL,
	[ChangedBy] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Job_Relations_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Job_Relations_Info](
	[UserId] [int] NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[RelationshipType] [nvarchar](50) NOT NULL,
	[Name] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_National_ID]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_National_ID](
	[PersonIdExternal] [int] NOT NULL,
	[CountryOrRegion] [nvarchar](50) NOT NULL,
	[NationalIdCardType] [nvarchar](50) NOT NULL,
	[NationalId] [nvarchar](50) NOT NULL,
	[IsPrimary] [nvarchar](50) NOT NULL,
	[LinkedAdhaar] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Pay_Component_Non_Recurring]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Pay_Component_Non_Recurring](
	[UserId] [int] NOT NULL,
	[Amount] [float] NOT NULL,
	[IssueDate] [datetime] NOT NULL,
	[PayComponent] [nvarchar](50) NOT NULL,
	[Currency] [nvarchar](50) NOT NULL,
	[SequenceNumber] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Pay_Component_Recurring]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Pay_Component_Recurring](
	[UserId] [int] NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[PayComponent] [nvarchar](50) NOT NULL,
	[Amount] [float] NOT NULL,
	[Currency] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Payment_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Payment_Info](
	[PaymentInformationWorker] [int] NOT NULL,
	[PaymentInformationEffectiveStartDate] [nvarchar](50) NOT NULL,
	[CountryOrRegionOrCode] [nvarchar](50) NOT NULL,
	[PayType] [nvarchar](50) NOT NULL,
	[PaymentMethod] [nvarchar](255) NOT NULL,
	[BankCountry] [nvarchar](50) NOT NULL,
	[Bank] [nvarchar](50) NULL,
	[RoutingNumber] [nvarchar](50) NOT NULL,
	[AccountOwner] [nvarchar](50) NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[Currency] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Personal_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Personal_Info](
	[PersonIdExternal] [int] NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](50) NULL,
	[Salutation] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](50) NULL,
	[FormalName] [nvarchar](50) NULL,
	[Gender] [nvarchar](1) NOT NULL,
	[MaritalStatus] [nvarchar](50) NULL,
	[MaritalStatusSince] [datetime] NULL,
	[NoOfChild] [int] NULL,
	[Nationality] [nvarchar](50) NULL,
	[SecondNationality] [nvarchar](50) NULL,
	[NativePreferredLanguage] [nvarchar](50) NULL,
	[ChallengeStatus] [nvarchar](50) NULL,
	[CertificateStartDate] [nvarchar](50) NULL,
	[CertificateEndDate] [nvarchar](50) NULL,
	[FatherName] [nvarchar](50) NULL,
	[BloodGroup] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Phone_Info]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Phone_Info](
	[PersonIdExternal] [int] NOT NULL,
	[PhoneType] [nvarchar](50) NOT NULL,
	[CountryRegionCode] [nvarchar](50) NULL,
	[AreaCode] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](12) NOT NULL,
	[Extension] [nvarchar](50) NULL,
	[IsPrimary] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dest_Work_Permit]    Script Date: 12/6/2021 11:32:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_Work_Permit](
	[UserId] [int] NOT NULL,
	[CountryOrRegion] [nvarchar](50) NULL,
	[DocumentType] [nvarchar](50) NULL,
	[DocumentTitle] [nvarchar](50) NULL,
	[DocumentNumber] [nvarchar](50) NULL,
	[IssueDate] [datetime] NOT NULL,
	[IssuePlace] [nvarchar](50) NULL,
	[IssuingAuthority] [nvarchar](50) NULL,
	[ExpirationDate] [datetime] NOT NULL,
	[Validated] [nvarchar](50) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dest_Biographical_Info]  WITH CHECK ADD FOREIGN KEY([CountryOrRegionOfBirth])
REFERENCES [dbo].[dest_Nationality_M] ([Country])
GO
ALTER TABLE [dbo].[dest_Biographical_Info]  WITH CHECK ADD FOREIGN KEY([CountryOfOrigin])
REFERENCES [dbo].[dest_Nationality_M] ([Country])
GO
ALTER TABLE [dbo].[dest_Personal_Info]  WITH CHECK ADD FOREIGN KEY([Gender])
REFERENCES [dbo].[dest_Gender_M] ([GenderKey])
GO
ALTER TABLE [dbo].[dest_Personal_Info]  WITH CHECK ADD FOREIGN KEY([Nationality])
REFERENCES [dbo].[dest_Nationality_M] ([Country])
GO
